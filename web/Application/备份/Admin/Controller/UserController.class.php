<?php 
	namespace Admin\Controller;
	
	//用户管理 控制器
	class UserController extends AdminController{

		private $_model = null; //数据库操作类
		private $_juese = null; //角色表操作类
		private $_user_juese = null; //用户——角色表操作类


		//初始化操作
		public function _initialize(){
			parent::_initialize();
			$this->_model = D('User');
			$this->_juese = D('Juese');
			$this->_user_juese = D('User_juese');
		}

		//加载 登录页的操作

		//执行登陆的操作
		// public function dologin(){
		// 	//进行查询
		// 	$user = $this->_model->where(array('username'=>array('eq',$_POST['username'])))->find();

		// 	//判断用户是否存在
		// 	if(!$user){
		// 		$this->error('用户 不存在！');
		// 		exit;
		// 	}

		// 	//判断密码是否正确
		// 	if($user['userpass'] != md5($_POST['pass'])){
		// 		$this->error('密码不正确!');
		// 		exit;
		// 	}

		// 	//执行登陆
		// 	$_SESSION['admin_user'] = $user;


		// 	//根据用户id获取对应的节点信息
		// 	$sql = "select n.mname,n.aname from lamp_user u join lamp_user_role ur on u.id=ur.user_id join lamp_role_node rn on ur.rid=rn.rid join lamp_node n on rn.nid=n.id where u.id={$user['id']}";
		// 	$list = M()->query($sql);
			
		// 	$nodelist = array();
		// 	$nodelist['Index'] = array('index');
		// 	//遍历重新拼装
		// 	foreach($list as $v){
		// 		$nodelist[$v['mname']][] = $v['aname'];
		// 		//把修改和执行修改 添加和执行添加 拼装到一起
		// 		if($v['aname']=="edit"){
		// 			$nodelist[$v['mname']][]="save";
		// 		}
		// 		if($v['aname']=="add"){
		// 			$nodelist[$v['mname']][]="doadd";
		// 		}
		// 	}
		// 	//将权限信息放置到session中
		// 	$_SESSION['admin_user']['nodelist'] = $nodelist;

		// 	//跳转首页
		// 	$this->redirect("Index/index");
		// }

		//列表详情
		public function index(){
			//查询数据
			$g=I('p',1,'int');
			$list = $this->_model->where('id')->page($g.',5')->select();
			$count = $this->_model->where('id')->count();// 查询满足要求的总记录数
			$Page = new \Think\Page($count,5);// 实例化分页类 传入总记录数和每页显示的记录数
			// $Page->setConfig('header','恰额王企鹅个会员');
	        $Page->setConfig('prev','上一页'); 
	        $Page->setConfig('next','下一页'); 
	        $Page->setConfig('first','首页'); 
	        $Page->setConfig('last','尾页'); 
	        // $Page->setConfig('theme',"共 %totalPage% 页$count %header% %first% %upPage% %linkPage% %downPage% %end%"); 
			$show = $Page->show();// 分页显示输出
	        $this->assign('list',$list);// 赋值数据集
	        $this->assign('page',$show);// 赋值分页输出
			$this->assign('count',$count);

			$arr = array(); //声明一个空数组
			//遍历用户信息
			foreach($list as $v){
				$juese_ids = $this->_user_juese->field('juese_id')->where(array('user_id'=>array('eq',$v['id'])))->select();
				//定义空数组
				$jueses = array();
				//遍历
				foreach($juese_ids as $value){
					$jueses[] = $this->_juese->where(array('id'=>array('eq',$value['juese_id']),'status'=>array('eq',1)))->getField('jname');
				}
				$v['juese'] = $jueses; //将新得到角色信息放置到$v中
				$arr[] = $v;
			}
			//分配变量
			$this->assign("list",$arr);

			//加载模板
			$this->display();
		}

		//执行添加操作
		public function doadd(){

			if(!$this->_model->create()){
				$this->error($this->_model->getError());
				exit;
			}

			if($this->_model->add() > 0){
				$this->success("添加成功！",U('User/index'));
			}else{
				$this->error("添加失败！");
			}
		}
		


		//删除操作
		public function del(){
			//把用户角色表中相关的也删除
			if($this->_model->delete($_GET['id']) > 0 && $this->_user_juese->where(array('user_id'=>array('eq',$_GET['id'])))->delete()){
				$this->success("删除成功！",U('User/index'));
			}else{
				$this->error("删除失败");
			}
		}

		//加载修改页面
		public function edit(){
			//查出数据
			$vo = $this->_model->where(array('id'=>array('eq',I('id'))))->find();
			//向模板分配数据
			$this->assign('vo',$vo);
			//加载模板
			$this->display();
		}

		//执行修改操作
		public function save(){
			
			//初始化
			if(!$this->_model->create()){
				$this->error($this->_model->getError());
				exit;
			}
			//执行修改 
			if($this->_model->save() >= 0){
				$this->success("修改成功！",U('User/index'));
			}else{
				$this->error("修改失败");
			}
		}


		//浏览 角色信息
		public function jueselist(){
			//查询节点信息
			$list = $this->_juese->where('status=1')->select();
			//查询当前用户信息
			$users = $this->_model->where(array('id'=>array('eq',I('id'))))->find();

			//获取当前用户的角色信息
			$jueselist = $this->_user_juese->where(array('user_id'=>array('eq',I('id'))))->select();

			$myjuese = array(); //定义空数组
			//对用户的角色进行重组
			foreach($jueselist as $v){
				$myjuese[] = $v['juese_id'];
			}
			

			//分配数据
			$this->assign("list",$list);
			//分配当前用户信息
			$this->assign('users',$users);
			//分配该用户的角色信息
			$this->assign('juese',$myjuese);

			//加载模板
			$this->display();
		}

		//保存用户角色
		public function savejuese(){
			
			//判读必须选择一个角色
			if(empty($_POST['juese'])){
				$this->error("请选择一个角色！");
			}

			$user_id = $_POST['user_id'];

			//清除用户所有的角色信息，避免重复添加
			$this->_user_juese->where(array('user_id'=>array('eq',$user_id)))->delete();
	
			foreach(I('juese') as $v){
				$data['user_id'] = $user_id;
				$data['juese_id'] = $v;
				//执行添加
				$this->_user_juese->data($data)->add();

			}

			$this->success("角色分配成功",U('User/index'));
			
		} 
		


}
		


