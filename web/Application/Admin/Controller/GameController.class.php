<?php
namespace Admin\Controller;
// use Think\Controller;
class GameController extends AdminController 
{
    public function index()
    {
        $User = M('Games'); // 实例化User对象
        $count = $User->count();// 查询满足要求的总记录数
        $Page = new \Think\Page($count,5);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = $User->order('id desc')->limit($Page->firstRow.','.$Page->listRows)->
        select();
        $this->assign('list',$list);// 赋值数据集
        $this->assign('show',$show);// 赋值分页输出
        $this->assign('title','游戏列表');
        $this->display('Game/index');
    }

    public function add()
    {
        $this->assign('title','用户列表');

        $this->display('Game/add');
    }

    public function doadd(){
        $data = M('Games');
            if(!$data->create()){
                $data->error($data->getError());
                exit;
            }

            if($data->add() > 0){
                $this->success("添加成功！",U('Game/index'));
            }else{
                $this->error("添加失败！");
            }

    }

    //执行删除
    public function del()
    {
        $userid = I('get.id');

        $User = D("Games"); // 实例化User对象
            if ($User->where("id = $userid")->delete() > 0) {
                $this->success('删除成功',U('Game/index'));
            } else {
                $this->error('删除失败');
            }

        // $User->where("id = $userid")->delete(); // 删除id为5的用户数据
    }

    //编辑页面
    public function edit($id)
    {
        //接收参数
        $id = I('get.id/d');
        //查数据
        // $da = D('Games')->find($id);
        $da = D('Games')->where(array('id'=>array('eq',I('id'))))->find();
        // V($da);
        // exit;
        $this->assign('title','编辑用户');
        $this->assign('da',$da);
        $this->display('Game/edit');
    }

    //执行修改
    public function update()
    {
        //判断 有无传递POST
        if (empty($_GET)) {
            $this->error('请完善数据!');
            exit;
        }
        //接收参数
        $da['id'] = I('get.id');
        $da['gname'] = I('get.gname');
        $da['msg'] = I('get.msg');
        $da['is_new'] = I('get.is_new');
        $da['display'] = I('get.display');
        M('Games')->create();//写入数据库
        //更新数据使用 save 方法
        if (M('Games')->save($da) > 0) {
            $this->success('编辑成功',U('Game/index'));
        } else {
            $this->error('编辑失败');
        }

}

}
