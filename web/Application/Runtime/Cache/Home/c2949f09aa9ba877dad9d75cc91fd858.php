<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>

<head>
	<title>007单机游戏介绍网  用玩心创造世界</title>
	<meta  charset="utf-8">
	<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
	<link href="/Public/twocss/style.css" rel="stylesheet" type="text/css">
	<script src="/Public/js/jquery-1.8.3.min.js"></script>
	<style type="text/css">
		#cards li{
		  display:none;
		}
		#cards li.active{
		  display:block;
		}
		*{list-style: none;margin:0;padding: 0;}

		#navs{background-color: #fff;height: 36px;width:100%; border-bottom: 2px solid #999;font-size: 15px;color:#000; line-height: 36px; cursor: pointer;}
		#navs div{padding-left:10px;}
		#navs div a{text-decoration: none}
	</style>
</head>
<body>

<?php if(empty($_SESSION['home'][0]['id'])): ?><div id="navs">
		<div style="float:left; margin-left:40px;">
			<a href="<?php echo U('Index/index');?>" style="color:#000;">首页</a>
		</div>
		<div style="float:left; margin-left:30px;">
			<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
		</div>
		<div style="float:right; margin-right:60px;">
			<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
		</div>
		<div style="float:right; margin-right:60px;">
			<a href="<?php echo U('Enter/enter');?>" style="color:#000;">|&nbsp;登录</a>
		</div>
		<div style="float:right; margin-right:10px;">
			<a href="<?php echo U('Login/login');?>" style="color:#000;">注册</a>
		</div>
		<div style="float:right; margin-right:60px;">
			<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
		</div>
	</div>
<?php else: ?>
	<div id="navs">
		<div style="float:left; margin-left:40px;">
			<a href="<?php echo U('Index/index');?>" style="color:#000;">首页</a>
		</div>
		<div style="float:left; margin-left:30px;">
			<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
		</div>
		<div style="float:right; margin-right:60px;">
			<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
		</div>
		<div style="float:right; margin-right:60px;">
			<a href="<?php echo U('Index/loginout');?>" style="color:#000;">退出</a>
		</div>
		<div style="float:right; margin-right:60px;">
			<a href="<?php echo U('Enter/enter');?>" style="color:#000;">切换账户</a>
		</div>

		<div style="float:right; margin-right:40px;">
			<div style="color:#000; margin-top:-5px;">
				<span style="color:#000;">欢迎您:&nbsp;</span>
				<a href="#" style="margin-right:50px;"><img src="/Public/img/avtar.png" width="25px" height="25px" style="margin-top:5px;"><span  style="color:#000;"><?php echo ($_SESSION['home'][0]['username']); ?></span></a>
				<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
			</div>
		</div>
	</div><?php endif; ?>

	<div id="page">
		<div id="header">
			<a href="<?php echo U('Index/index');?>"><img class="logo" src="/Public/twoimg/logo-1.png" style="margin-left:50px;"></a>
			
		<ul class="navigation" id="options">
			<li>
				<a style="cursor:pointer;"  href="<?php echo U('Sort/index');?>">武侠仙侠</a>
			</li>
			<li>
				<a style="cursor:pointer;" href="<?php echo U('Sort/jsby');?>">角色扮演</a>
			</li>
			<li>
				<a class="active" style="cursor:pointer;" href="<?php echo U('Sort/jszl');?>">即时战略</a>
			</li>
			<li>
				<a style="cursor:pointer;" href="<?php echo U('Sort/sj');?>">动作射击</a>
			</li>
			<li>
				<a style="cursor:pointer;" href="<?php echo U('Sort/js');?>">赛车竞速</a>
			</li>
		</ul>
	</div>
	<div id="body">
		<div class="featured"> <img src="/Public/img/07.jpg" width="100%"> <a href="<?php echo U('Index/index');?>" class="">007 单机 Game 首页</a> </div>
		<div id="content">
			<div class="content">
				<ul class="articles" id="cards">
				<?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vzl): $mod = ($i % 2 );++$i;?><li class="active">
						<h2> <a href="index.html">《<?php echo ($vzl["gname"]); ?>》</a> <span><span id="scimg" style="margin-left:260px; margin-top:10px; cursor:pointer;" onclick="ads(<?php echo ($vzl["gid"]); ?>);"><img src="/Public/img/sc3.jpg" width="30px" height="30px;" title="点我收藏该游戏"></span><?php echo ($titles); ?></span> </h2>
						<a href="index.html"><img src="/Uploads/<?php echo ($vzl["savepath"]); ?>/<?php echo ($vzl["savename"]); ?>"  width="200px" height="110px"></a>
						<p><?php echo ($vzl["msg"]); ?></p>
					</li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
				<script type="text/javascript">
						function ads(gid){
							alert(gid);
							//自定义动画
							$('#scimg img').click(function(){
							    $(this).parent().fadeToggle(1000)
							})

							$.ajax({
								type:'get',
								data:{gid:gid},
								// dataType:'json',
								url:"<?php echo U('Sort/scyx');?>",
								success:function(data){
									// console.log(data);
									if (data > 0) {
										alert('收藏成功~!');
									} else {
										alert('请检查您是否登录或已经收藏!~');
									}
								},
							})
						}
				</script>
	

					<!--选项卡-->
					<script type="text/javascript">
					    //找对象  改属性
					    var op = document.getElementById('options').getElementsByTagName('a');
					    // console.log(op);
					    var ca = document.getElementById('cards').getElementsByTagName('li');
					    // console.log(ca);
					    for (var i = 0; i < op.length; i++) {
					      (function(i){
					        op[i].onclick = function(){
					          for(var m = 0; m<op.length; m++){
					            op[m].className = "";
					            ca[m].className = "";
					          }
					          op[i].className = 'active';
					          ca[i].className = 'active';
					        }
					      })(i)
					    };
					</script>
					<ul class="paging">
						<li>
							<?php echo ($page); ?>
						</li>
					</ul>
				</div>
				<div id="sidebar">
					<h1><span>007 Game</span></h1>
					<ul class="items" id="newslist">
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (2).png" alt=""></a>
							<h2><a href="index.html">仙剑奇侠传5</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (1).png" alt=""></a>
							<h2><a href="index.html">仙剑奇侠传6</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (3).png" alt=""></a>
							<h2><a href="index.html">武神赵子龙</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (4).png" alt=""></a>
							<h2><a href="index.html">雪鹰领主</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (5).png" alt=""></a>
							<h2><a href="index.html">我的世界</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (6).png" alt=""></a>
							<h2><a href="index.html">鬼泣5</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (7).png" alt=""></a>
							<h2><a href="index.html">真三国无双7</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (8).png" alt=""></a>
							<h2><a href="index.html">仙剑5前传</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (9).png" alt=""></a>
							<h2><a href="index.html">三国之13</a></h2>
						</li>
						<li>
							<a href="index.html"><img src="/Public/img/80x80 (10).png" alt=""></a>
							<h2><a href="index.html">极品飞车9</a></h2>
						</li>
					</ul>
					<script>
					    $(function(){
					        setInterval(function(){
					            $('#newslist li').last().fadeTo(0,0).hide().prependTo('#newslist').slideDown(1000).fadeTo(1000,1);
					        },3000)
					    })
					</script>
					<a href="#"  class="more">&nbsp;回顶部</a> 
				</div>
			</div>
		</div>
		<div id="footer"> <span>&copy; 2016 <a href="<?php echo U('Index/index');?>">LSW神一般的队友</a>. 007小组.&nbsp;<a href="<?php echo U('Index/index');?>" title="007单机游戏网" target="_blank">&007单机游戏网</a></span> </div>
	</div>
</body>
</html>