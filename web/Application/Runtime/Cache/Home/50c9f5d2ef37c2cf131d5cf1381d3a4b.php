<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8">
        <title>007 单机游戏网</title>
    <meta name="keywords" content="007 单机游戏网 用玩心创造世界" />
    <meta name="description" content="007 单机游戏网 用玩心创造世界 总有一款适合你" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <script src="/Public/assets/js/jquery-1.8.2.min.js"></script>
<!--         <script src="/Public/assets/js/supersized.3.2.7.min.js"></script>
        <script src="/Public/assets/js/supersized-init.js"></script>
        <script src="/Public/assets/js/scripts.js"></script> -->
        <!-- CSS -->
        <link rel="stylesheet" href="/Public/assets/css/reset.css">
        <link rel="stylesheet" href="/Public/assets/css/supersized.css">
        <link rel="stylesheet" href="/Public/assets/css/style.css">


        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
<style type="text/css">
    #bg1{
        background-image: url('/Public/img/A9.jpg');
        background-size: 100% 100%;

    }
    ul#nav{ width:100%; height:30px; background: rgba(45,45,45,.15); margin:0 auto;} 
    ul#nav li{display:inline; height:30px; float: left;} 
    ul#nav li a{display:inline-block; padding:0 20px; height:30px; line-height:30px; float: left;
      font-size:20px; text-decoration: none; color:#fff;} 
    ul#nav li a:hover{background: rgba(45,45,45,.15)}/*设置鼠标滑过或悬停时变化的背景颜色*/ 
</style>
    </head>

    <body id="bg1" >
      <ul id="nav"> 
          <li><a href="<?php echo U('Index/index');?>">首页</a></li> 
          <li><a href="<?php echo U('Gallery/index');?>">图库</a></li> 
      </ul> 
        <div class="page-container" style="margin-top:10px">
            <h1 style="color:#fff;">注册</h1>

            <form  action="<?php echo U('Login/insert');?>" method="post" name="regForm" onsubmit="return checkForm()">
            <table class="table">
            <a href="<?php echo U('Enter/enter');?>" style="text-decoration: none; font-size:15px; color:#fff; ">已有账号点此登录</a>
                <tr>
                    <td><input type="text"  class="form-control" name="username" onblur="checkUser() " placeholder="请输入用户名"><span id="utab" style="color:#fff;">* 必填项,(6-12位 数字字母下划线)</span></td>
                </tr>
                <tr>
                    <td><input type="password" class="form-control" name="userpass" onblur="checkPass()" placeholder="请输入密码"><span id="ptab" style="color:#fff;">* 必填项,请输入5-18位的密码</span></td>
                </tr>
                <tr>
                    <td><input type="password" class="form-control" name="userrepass" onblur="checkRepwd()" placeholder="确认密码"><span id="rptab" style="color:#fff;">* 必填项,请再次输入密码</span></td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" name="tel" onblur="checkTel()" placeholder="请输入手机号"><span id="ttab" style="color:#fff;">* 必填项,请再次手机号</span></td>
                </tr>
                <tr>
                    <td><input type="text" class="form-control" name="email" onblur="checkEmail()" placeholder="请输入邮箱"><span id="etab" style="color:#fff;">* 必填项, 请输入常用邮箱</span></td>
                </tr>
                <tr>
                    <td ><input type="text" name="code" placeholder="验证码" style="width:100px; margin-left:-10px;"><img src="<?php echo U('Yzm/action');?>" height="40px" width="100px" title="点我刷新" style="padding-left:70px;margin-top:20px; cursor:pointer" onclick="this.src=this.src+'?i='+Math.random()"></td>
                </tr>
                <tr>
                    <td><button type="submit">立即注册</button></td>
                </tr>
            </table>
            </form>
            <div class="connect">
                <p style="color:#fff;">Or connect with:</p>
                <p>
                    <a style="color:#fff;" class="facebook" href=""></a>
                    <a style="color:#fff;" class="twitter" href=""></a>
                </p>
            </div>
        </div>
    
        <!-- Javascript -->

        <script>
            //定义验证邮箱的函数
            function checkEmail(){
                var email = document.regForm.email.value;
                var etab = document.getElementById('etab');
                //做判断
                if (email.match(/^[\w-]+@[\w-]+(\.\w+){1,3}$/) == null) {
                    etab.innerHTML = '* 邮箱格式不正确!';
                    etab.style.color = '#f00';
                    return false;
                } else{
                    etab.innerHTML = '* √验证通过!';
                    etab.style.color = '#0a0';
                    return true;
                }
            }

            //定义验证用户名的函数
            function checkUser(){
                var name = document.regForm.username.value;
                var utab = document.getElementById('utab');
                //做判断

                if (name.match(/^\w{5,12}$/) == null) {
                    utab.innerHTML = '* 用户名由5-12位 数字字母下划线组成!';
                    utab.style.color = '#f00';
                    return false;
                } else{
                    utab.innerHTML = '* √验证通过!';
                    utab.style.color = '#0a0';
                    return true;
                }
            }

            //定义验证手机号名的函数
            function checkTel(){
                var tel = document.regForm.tel.value;
                var ttab = document.getElementById('ttab');
                //做判断
                if (tel.match(/^\w{11}$/) == null) {
                    ttab.innerHTML = '* 用户名由11位 数字组成!';
                    ttab.style.color = '#f00';
                    return false;
                } else{
                    ttab.innerHTML = '* √验证通过!';
                    ttab.style.color = '#0a0';
                    return true;
                }
            }

            //定义验证密码的函数
            function checkPass(){
                var pwd = document.regForm.userpass.value;
                var ptab = document.getElementById('ptab');
                //做判断
                if (pwd.length <6 || pwd.length >18) {
                    ptab.innerHTML = '* 密码不合法!';
                    ptab.style.color = '#f00';
                    return false;
                } else{
                    ptab.innerHTML = '* √验证通过!';
                    ptab.style.color = '#0a0';
                    return true;
                }
            }

            //定义验证密码的函数
            function checkRepwd(){
                var pwd = document.regForm.userpass.value;
                var repwd = document.regForm.userrepass.value;
                var rptab = document.getElementById('rptab');
                //做判断
                if (pwd != repwd) {
                    rptab.innerHTML = '* 两次密码不一致!';
                    rptab.style.color = '#f00';
                    return false;
                } else{
                    rptab.innerHTML = '* √验证通过!';
                    rptab.style.color = '#0a0';
                    return true;
                }
            }
            
            function checkForm(){
                return checkEmail() && checkUser() && checkPass() &&checkRepwd()
            }


            
        </script>
    </body>

</html>