<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html lang="cn">
<head>
<title>007单机游戏介绍网  用玩心创造世界</title>
<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jolly Games Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<link href="/Public/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="/Public/css/swipebox.css" rel='stylesheet' type='text/css' />
<link href="/Public/css/style.css" rel='stylesheet' type='text/css' />	
<link href="/Public/my.css" rel='stylesheet' type='text/css' />	
<!-- Custom Theme files -->
<script src="/Public/js/jquery.min.js"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<style type="text/css">
	#bg1{
	    background-color: #eee;
	}
	#bg2{
		background-image: url('/Public/img/01.jpg');
	}
	#as{
	    width:230px;
	    height:40px;
	    background-color:#abcdef;
	}
	*{list-style: none;margin:0;padding: 0;}
	#header{width:650px; }
	#play{width:650px;  background-color: #fff;position: absolute;left:150px;top:190px;  z-index: 999999999999; border-left:1px solid #ddd;}
	#imgs{float: right;}
	#iconlist{ width:250px; height:260px; background-color: #fff;}
	#iconlist li{width:250px; height:52px; text-align: left; font-size: 18px; line-height: 50px;cursor: pointer;}

	#navs{background-color: #fff;height: 36px;width:100%; border-bottom: 2px solid #999;font-size: 15px;color:#000; line-height: 36px; cursor: pointer;}
</style>

</head>
<body>
	<audio controls="controls" autoplay="autoplay" style="display: none">
		  <!-- <source src="/Public/music.mp3" type="audio/ogg" /> -->
	</audio><!--音乐播放-->
	<!--start-home-->
	<?php if(empty($_SESSION)): ?><div id="navs">
			<div style="float:left; margin-left:40px;">
				<a href="<?php echo U('Index/index');?>" style="color:#000;">首页</a>
			</div>
			<div style="float:left; margin-left:30px;">
				<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Enter/enter');?>" style="color:#000;">|&nbsp;登录</a>
			</div>
			<div style="float:right; margin-right:10px;">
				<a href="<?php echo U('Login/login');?>" style="color:#000;">注册</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
			</div>
		</div>
	<?php else: ?>
		<div id="navs">
			<div style="float:left; margin-left:40px;">
				<a href="<?php echo U('Index/index');?>" style="color:#000;">首页</a>
			</div>
			<div style="float:left; margin-left:30px;">
				<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Index/loginout');?>" style="color:#000;">退出</a>
			</div>

			<div style="float:right; margin-right:40px;">
				<div style="color:#000;">
					<span style="color:#000;">欢迎您:</span>
					<a href="#" style="margin-right:50px;"><img src="/Public/img/avtar.png" width="30px" height="30px"><span  style="color:#000;"><?php echo ($_SESSION['home'][0]['username']); ?></span></a>
					<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
				</div>
			</div>
		</div><?php endif; ?>



	<div id="home" class="header">

		<div class="header-top">
			<div class="container">
				<div class="head-nav">
					<div class="logo">
						<a href="<?php echo U("Index/index");?>"><h1>007<span>单机Game<?php echo ($title); ?></span></h1></a>
					</div>
					<p>007单机游戏介绍网  用玩心创造世界</p>
					<div class="clearfix"></div>
					<span class="menu"></span>
					<div class="top-menu">
						<ul>
						<li class="active"><a class="color1" href="<?php echo U('Index/index');?>"  >首页</a></li>
							<li><a class="color2" href="<?php echo U('Sort/index');?>" id="yxx">游戏分类<span class="glyphicon glyphicon-chevron-down"></span></a></li>
							<li><a class="color5" href="<?php echo U('Gallery/gallery');?>">图库</a></li>
							<li><a class="color3" href="typography.html">玩家社区</a></li>
							<li><a href="##" class="color4">客服</a></li>
							<li><a class="color6" href="<?php echo U('Admin/Public/login');?>">后台入口</a></li>
							<div class="clearfix"> </div>
						</ul>
					</div>
					<!--script-for-menu-->
					<script>
						$( "span.menu" ).click(function() {
						  $( ".top-menu" ).slideToggle( "slow", function() {
						    // Animation complete.
						  });
						});
					</script>
					<!--script-for-menu-->
				</div>
			</div>
		</div> 

		<!-- banner-bottom -->
		<div class="banner">

				  <!--分类列表-->
				  <div id="header" style="display:none;">
				      <div id="play" >
				        <div id="imgs">
				            <a href="<?php echo U('Sort/index');?>"><img style="display:block" src="/Public/img/A1.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/jsby');?>"><img style="display:none" src="/Public/img/A2.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/jszl');?>"><img style="display:none" src="/Public/img/A3.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/sj');?>"><img style="display:none" src="/Public/img/A4.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/js');?>"><img style="display:none" src="/Public/img/A5.jpg" width="400px" height="260px"></a>
				        </div>
				        <div>
				              <ul id="iconlist">
				                  <li><a class="active" style="cursor:pointer;"  href="<?php echo U('Sort/index');?>">武侠仙侠</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/jsby');?>">角色扮演</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/jszl');?>">即时战略</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/sj');?>">动作射击</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/js');?>">竞速</a></li>
				              </ul>
				        </div>
				      </div>
				  </div>
				  <!--分类列表-->
				  <script type="text/javascript">
				      $header = $('#header');
				      $iconlist = $('#iconlist li');
				      $imglist = $('#imgs img');

				      //控制heder  div
				      $('#yxx').mouseenter(function(){
				          $($header).slideToggle();
				        });

				      //设置循环 
				      var m = 0;

				      //控制图片 
				      function img(m){
				          for (var i =0; i < $imglist.length; i++) {
				              $imglist[i].style.display = 'none';
				          }
				          $imglist[m].style.display = 'block';
				      }

				      //控制按钮 
				      function icon(m){
				          for (var i =0; i < $iconlist.length; i++) {
				              $iconlist[i].style.backgroundColor = '#fff';
				          }
				          $iconlist[m].style.backgroundColor = '#eee';
				      }

				      //文字控制
				      for (var i = 0; i < $iconlist.length; i++) {
				          (function(i){
				              $iconlist[i].onmouseover = function(){
				                  img(i);
				                  icon(i);
				                  //改变循环变量
				                  m = i + 1;
				              }
				          })(i)
				      };
				  </script>
				  <!--分类列表-->
				  
				  <!-- Slider-starts-Here -->
				  <script src="/Public/js/responsiveslides.min.js"></script>
				  	<script>
				  	    // You can also use "$(window).load(function() {"
				  	    $(function () {
				  	      // Slideshow 4
				  	      $("#slider3").responsiveSlides({
				  	        auto: true,
				  	        pager:true,
				  	        nav:false,
				  	        speed: 500,
				  	        namespace: "callbacks",
				  	        before: function () {
				  	          $('.events').append("<li>before event fired.</li>");
				  	        },
				  	        after: function () {
				  	          $('.events').append("<li>after event fired.</li>");
				  	        }
				  	      });
				  	
				  	    });
				  	  </script>
			<!--//End-slider-script -->
				<div  id="top" class="callbacks_container">
					<ul class="rslides" id="slider3">
						<li>
							<img class="img-responsive" src="/Public/img/1.jpg" >
						</li>
						<li>
							<img class="img-responsive" src="/Public/img/05.jpg">
						</li>
						<li>
							<img class="img-responsive" src="/Public/img/3.jpg">
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!--banner-bottom-->
		<div class="banner-bottom">
			<div class="container">
			 <div class="banner-bot-grids">
				 <div class="col-md-4 banner-grid one">
				      <div class="col-md-3 icon">
						<i class="s1"> </i>
					</div>
					<div class="col-md-9 icon-text">
					    <h4>每日新闻</h4>
						<p>先占个位.</p>
					 </div>
					  <div class="clearfix"> </div>
				 </div>
				 <div class="col-md-4 banner-grid two">
				      <div class="col-md-3 icon">
						<i class="s2"> </i>
					</div>
					<div class="col-md-9 icon-text">
					     <h4>每日新闻</h4>
						<p>先占个位.</p>
					 </div>
					  <div class="clearfix"> </div>
				 </div>
				 <div class="col-md-4 banner-grid three">
				     <div class="col-md-3 icon">
						<i class="s3"> </i>
					</div>
					<div class="col-md-9 icon-text">
					     <h4>每日新闻</h4>
						<p>先占个位.</p>
					 </div>
					  <div class="clearfix"> </div>
				 </div>
			 </div>
			 <div class="clearfix"> </div>
		</div>
	</div>
	<!--latest-post-->
		<!--top-games-section-->
				  <div class="top-games-section">
	 				 <div class="container">
						<!--sreen-gallery-cursual-->
						<div class="col-md-3 top-games">
						      <h3>每日推荐游戏</h3>
							  <p>游戏一的介绍</p>
							   <p>游戏2的介绍</p>
							   <p>游戏3的介绍</p>
						</div>
						<div class="col-md-9 g-views">
							<ul id="flexiselDemo3">
								<li>
								<div class="biseller-column">
								<a class="lightbox" href="#goofy">
											   <img src="/Public/img/s1.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												  <img src="/Public/img/s1.jpg"/>
											   <a class="lightbox-close" href="#"> </a>

												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
							<li>
								<div class="biseller-column">
								<a class="lightbox" href="#goofy">
											  <img src="/Public/img/s2.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												  <img src="/Public/img/s2.jpg"/>
											   <a class="lightbox-close" href="#"> </a>

												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
							<li>
								<div class="biseller-column">
								<a class="lightbox" href="#goofy">
											  <img src="/Public/img/s3.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												  <img src="/Public/img/s3.jpg"/>
											   <a class="lightbox-close" href="#"> </a>

												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
							<li>
								<div class="biseller-column">
								<a class="lightbox" href="#goofy">
												<img src="/Public/img/s1.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												<img src="/Public/img/s1.jpg"/>
											   <a class="lightbox-close" href="#"> </a>

												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
							<li>
								<div class="biseller-column">
								<a class="lightbox" href="#goofy">
											  <img src="/Public/img/s2.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												  <img src="/Public/img/s2.jpg"/>
											   <a class="lightbox-close" href="#"> </a>

												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
							<li>
								<div class="biseller-column">
								<a class="lightbox" href="#goofy">
											   <img src="/Public/img/s1.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												<img src="/Public/img/s1.jpg"/>
											   <a class="lightbox-close" href="#"> </a>

												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
							<li>
								<div class="biseller-column">
											<a class="lightbox" href="#goofy">
												<img src="/Public/img/s2.jpg"/>
											</a> 
											<div class="lightbox-target" id="goofy">
												<img src="/Public/img/s2.jpg"/>
											   <a class="lightbox-close" href="#"> </a>
												
												<div class="clearfix"> </div>
											</div>
								</div>
							</li>
						</ul>
					<script type="text/javascript">
						 $(window).load(function() {
							$("#flexiselDemo3").flexisel({
								visibleItems:3,
								animationSpeed: 1000,
								autoPlay: true,
								autoPlaySpeed: 3000,    		
								pauseOnHover: true,
								enableResponsiveBreakpoints: true,
								responsiveBreakpoints: { 
									portrait: { 
										changePoint:480,
										visibleItems:3
									}, 
									landscape: { 
										changePoint:640,
										visibleItems:3
									},
									tablet: { 
										changePoint:768,
										visibleItems:3
									}
								}
							});
							
						});
					   </script>
					   <script type="text/javascript" src="/Public/js/jquery.flexisel.js"></script>
					</div>   
			</div>
	</div>
	<!--start-latest-->
		<div class="latest-post">
			<div class="container">
			        <h3>Latest Games</h3>
			<div class="grid">
				<figure class="effect-hera">
						<img src="/Public/img/l1.jpg" alt="img07"/>
						<figcaption>
							<h2>热门 <span>游戏</span></h2>
							<p>
								<a href="#"><i class="download"></i></a>
								<a href="#"><i class="heart"></i></a>
								<a href="#"><i class="service"></i></a>
								<a href="#"><i class="share"></i></a>
							</p>
						</figcaption>			
					</figure>
					<figure class="effect-hera">
						<img src="/Public/img/l2.jpg" alt="img07"/>
						<figcaption>
							<h2>热门 <span>游戏</span></h2>
							<p>
								<a href="#"><i class="download"></i></a>
								<a href="#"><i class="heart"></i></a>
								<a href="#"><i class="service"></i></a>
								<a href="#"><i class="share"></i></a>
							</p>
						</figcaption>			
					</figure>
					<figure class="effect-hera">
						<img src="/Public/img/l3.jpg" alt="img07"/>
						<figcaption>
							<h2>热门 <span>游戏</span></h2>
							<p>
								<a href="#"><i class="download"></i></a>
								<a href="#"><i class="heart"></i></a>
								<a href="#"><i class="service"></i></a>
								<a href="#"><i class="share"></i></a>
							</p>
						</figcaption>			
					</figure>
					<figure class="effect-hera">
						<img src="/Public/img/l4.jpeg" alt="img07"/>
						<figcaption>
							<h2>热门 <span>游戏</span></h2>
							<p>
								<a href="#"><i class="download"></i></a>
								<a href="#"><i class="heart"></i></a>
								<a href="#"><i class="service"></i></a>
								<a href="#"><i class="share"></i></a>
							</p>
						</figcaption>			
					</figure>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	<!--start-new-trailers-->
		<div class="new-trailers">
	    <div class="container">
				<h3>New Trailers</h3>
			 <div class="trailer-top">
				<div class="col-md-7 trailer">
					 <iframe src="https://www.youtube.com/embed/QKLSGALIWUE" frameborder="0" allowfullscreen></iframe>
				</div>
				<div class="col-md-5 trailer-text">
				   <div class="sub-trailer">
				       <div class="col-md-4 sub-img">
							<img src="/Public/img/v2.jpg" alt="img07"/>
					   </div>
					   <div class="col-md-8 sub-text">
					   		 <a href="#">Killzone: Shadow Fall for PlayStation 4 Reviews</a>
							 <p>Lorem ipsum dolor sit amet, consectetur adipi…</p>
					   </div>
					    <div class="clearfix"> </div>
				   </div>
				    <div class="sub-trailer">
				       <div class="col-md-4 sub-img">
							<img src="/Public/img/v1.jpg" alt="img07"/>
					   </div>
					   <div class="col-md-8 sub-text">
					   		 <a href="#"> Spiderman 2 Full Version PC Game</a>
							 <p>Lorem ipsum dolor sit amet, consectetur adipi…</p>
					   </div>
					    <div class="clearfix"> </div>
				   </div>
				    <div class="sub-trailer">
				       <div class="col-md-4 sub-img">
							<img src="/Public/img/v3.jpg" alt="img07"/>
					   </div>
					   <div class="col-md-8 sub-text">
					   		 <a href="#">Killzone: Shadow Fall for PlayStation 4 Reviews</a>
							 <p>Lorem ipsum dolor sit amet, consectetur adipi…</p>
					   </div>
					    <div class="clearfix"> </div>
				   </div>
				</div>
				 <div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!--start-blog-->
		<div id="blog" class="blog-section">
		   <div class="container">
		         <h3>Blog</h3>
				 <div class="blog">
				   <div class="col-md-4 blog-text">
					   <h5>THU 14 May, 2015</h5>
					   <a href="single.html"><h4>EXCEPTEUR SINT OCCAECAT CUPIDATAT NON</h4></a>
					   <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				   </div>
					<div class="col-md-8 welcome-img">
					 <a href="single.html" class="mask"><img src="/Public/img/b2.jpg" alt="image" class="img-responsive zoom-img"></a>
					</div>
				   <div class="clearfix"> </div>
			   </div>
			    <div class="blog">
				 <div class="col-md-4 blog-text two">
					   <h5>THU 14 May, 2015</h5>
					  <a href="single.html"><h4>LOREM IPSUM DOLOR SIT AMET CONSE</h4></a>
					   <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				   </div>
					<div class="col-md-8 blog-img two">
					 <a href="single.html" class="mask"><img src="/Public/img/b3.jpg" alt="image" class="img-responsive zoom-img"></a>
					</div>
				   <div class="clearfix"> </div>
			   </div> 
			    <div class="blog">
				   <div class="col-md-4 blog-text">
					   <h5>THU 14 May, 2015</h5>
					   <a href="single.html"><h4>EXCEPTEUR SINT OCCAECAT CUPIDATAT NON</h4></a>
					   <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				   </div>
					<div class="col-md-8 welcome-img">
					 <a href="single.html" class="mask"><img src="/Public/img/b1.jpg" alt="image" class="img-responsive zoom-img"></a>
					</div>
				   <div class="clearfix"> </div>
			   </div>
		</div>
	</div>
	<!--block-->
   <!--footer-->
<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
		<div class="footer">
				<div class="container">
					<div class="footer-grids">
						<div class="col-md-12 footer-text">  
							<h3>007 Game</h3>
							<hr>
							<p>007游戏　版权所有　文化部网络游戏举报和联系电子邮箱：wlwh@vip.sina.com 纠纷处理方式：联系客服或依《用户协议》约定方式处理三七互娱旗下·上海硬通网络科技有限公司 沪网文[2014]0024-024号 沪ICP备14000728号-2 增值电信业务经营许可证沪B2-20140017</p>
							<a class="mt20" href="#">
								<img src="/Public/img/last1.jpg" width="171px" height="51px">
								<img src="/Public/img/last2.jpg" width="171px" height="51px">
								<img src="/Public/img/last3.jpg" width="171px" height="51px">
								<img src="/Public/img/last4.jpg" width="171px" height="51px">
								<img src="/Public/img/last5.jpg" width="171px" height="51px">
								<img src="/Public/img/last6.jpg" width="171px" height="51px">
							</a>
						</div>

						<div class="clearfix"> </div>
					</div>
					<div class="copy">
						<p>抵制不良单机游戏，拒绝盗版游戏。 注意自我保护，谨防受骗上当。 适度游戏益脑，沉迷游戏伤身。 合理安排时间，享受健康生活。</p>
						<p>&copy; 2016 007 小组 <a href="http://w3layouts.com/">007单机Game网</a> </p>
					</div>
				</div>
			</div>
			<!--start-smoth-scrolling-->
			<script type="text/javascript">
								jQuery(document).ready(function($) {
									$(".scroll").click(function(event){		
										event.preventDefault();
										$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
									});
								});
								</script>
							<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										// $().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>