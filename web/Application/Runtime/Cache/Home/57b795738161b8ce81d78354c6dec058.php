<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html lang="cn">
<head>
<title>007单机游戏介绍网  用玩心创造世界</title>
<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jolly Games Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<link href="/Public/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="/Public/css/swipebox.css" rel='stylesheet' type='text/css' />
<link href="/Public/css/style.css" rel='stylesheet' type='text/css' />	
<link href="/Public/my.css" rel='stylesheet' type='text/css' />	
<!-- Custom Theme files -->
<script src="/Public/js/jquery.min.js"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<style type="text/css">
	#bg1{
	    background-color: #eee;
	}
	#bg2{
		background-image: url('/Public/img/01.jpg');
	}
	#as{
	    width:230px;
	    height:40px;
	    background-color:#abcdef;
	}
	*{list-style: none;margin:0;padding: 0;}
	#header{width:650px; }
	#play{width:650px;  background-color: #fff;position: absolute;left:150px;top:190px;  z-index: 999999999999; border-left:1px solid #ddd;}
	#imgs{float: right;}
	#iconlist{ width:250px; height:260px; background-color: #fff;}
	#iconlist li{width:250px; height:52px; text-align: left; font-size: 18px; line-height: 50px;cursor: pointer;}

	#navs{background-color: #fff;height: 36px;width:100%; border-bottom: 2px solid #999;font-size: 15px;color:#000; line-height: 36px; cursor: pointer;}
</style>

</head>
<body>
	<audio controls="controls" autoplay="autoplay" style="display: none">
		  <!-- <source src="/Public/music.mp3" type="audio/ogg" /> -->
	</audio><!--音乐播放-->
	<!--start-home-->
	<?php if(empty($_SESSION)): ?><div id="navs">
			<div style="float:left; margin-left:40px;">
				<a href="#" style="color:#000;">首页</a>
			</div>
			<div style="float:left; margin-left:30px;">
				<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Enter/enter');?>" style="color:#000;">|&nbsp;登录</a>
			</div>
			<div style="float:right; margin-right:10px;">
				<a href="<?php echo U('Login/login');?>" style="color:#000;">注册</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
			</div>
		</div>
	<?php else: ?>
		<div id="navs">
			<div style="float:left; margin-left:40px;">
				<a href="#" style="color:#000;">首页</a>
			</div>
			<div style="float:left; margin-left:30px;">
				<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Index/loginout');?>" style="color:#000;">退出</a>
			</div>

			<div style="float:right; margin-right:40px;">
				<div style="color:#000;">
					<span style="color:#000;">欢迎您:</span>
					<a href="#" style="margin-right:50px;"><img src="/Public/img/avtar.png" width="30px" height="30px"><span  style="color:#000;"><?php echo ($_SESSION['home'][0]['username']); ?></span></a>
					<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
				</div>
			</div>
		</div><?php endif; ?>



	<div id="home" class="header">

		<div class="header-top">
			<div class="container">
				<div class="head-nav">
					<div class="logo">
						<a href="<?php echo U("Index/index");?>"><h1>007<span>单机Game<?php echo ($title); ?></span></h1></a>
					</div>
					<p>007单机游戏介绍网  用玩心创造世界</p>
					<div class="clearfix"></div>
					<span class="menu"></span>
					<div class="top-menu">
						<ul>
						<li class="active"><a class="color1" href="<?php echo U('Index/index');?>"  >首页</a></li>
							<li><a class="color2" href="<?php echo U('Sort/index');?>" id="yxx">游戏分类<span class="glyphicon glyphicon-chevron-down"></span></a></li>
							<li><a class="color5" href="<?php echo U('Gallery/gallery');?>">图库</a></li>
							<li><a class="color3" href="typography.html">玩家社区</a></li>
							<li><a href="##" class="color4">客服</a></li>
							<li><a class="color6" href="<?php echo U('Admin/Public/login');?>">后台入口</a></li>
							<div class="clearfix"> </div>
						</ul>
					</div>
					<!--script-for-menu-->
					<script>
						$( "span.menu" ).click(function() {
						  $( ".top-menu" ).slideToggle( "slow", function() {
						    // Animation complete.
						  });
						});
					</script>
					<!--script-for-menu-->
				</div>
			</div>
		</div> 

		<!-- banner-bottom -->
		<div class="banner">

				  <!--分类列表-->
				  <div id="header" style="display:none;">
				      <div id="play" >
				        <div id="imgs">
				            <a href="<?php echo U('Sort/index');?>"><img style="display:block" src="/Public/img/A1.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/jsby');?>"><img style="display:none" src="/Public/img/A2.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/jszl');?>"><img style="display:none" src="/Public/img/A3.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/sj');?>"><img style="display:none" src="/Public/img/A4.jpg" width="400px" height="260px"></a>
				            <a href="<?php echo U('Sort/js');?>"><img style="display:none" src="/Public/img/A5.jpg" width="400px" height="260px"></a>
				        </div>
				        <div>
				              <ul id="iconlist">
				                  <li><a class="active" style="cursor:pointer;"  href="<?php echo U('Sort/index');?>">武侠仙侠</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/jsby');?>">角色扮演</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/jszl');?>">即时战略</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/sj');?>">动作射击</a></li>
				                  <li><a style="cursor:pointer;" href="<?php echo U('Sort/js');?>">竞速</a></li>
				              </ul>
				        </div>
				      </div>
				  </div>
				  <!--分类列表-->
				  <script type="text/javascript">
				      $header = $('#header');
				      $iconlist = $('#iconlist li');
				      $imglist = $('#imgs img');

				      //控制heder  div
				      $('#yxx').mouseenter(function(){
				          $($header).slideToggle();
				        });

				      //设置循环 
				      var m = 0;

				      //控制图片 
				      function img(m){
				          for (var i =0; i < $imglist.length; i++) {
				              $imglist[i].style.display = 'none';
				          }
				          $imglist[m].style.display = 'block';
				      }

				      //控制按钮 
				      function icon(m){
				          for (var i =0; i < $iconlist.length; i++) {
				              $iconlist[i].style.backgroundColor = '#fff';
				          }
				          $iconlist[m].style.backgroundColor = '#eee';
				      }

				      //文字控制
				      for (var i = 0; i < $iconlist.length; i++) {
				          (function(i){
				              $iconlist[i].onmouseover = function(){
				                  img(i);
				                  icon(i);
				                  //改变循环变量
				                  m = i + 1;
				              }
				          })(i)
				      };
				  </script>
				  <!--分类列表-->
				  
<link type="text/css" rel="stylesheet" href="/Public/center/my.min.css">
<style type="text/css">
    /**{font-family: Microsoft YaHei,'宋体' , Tahoma, Helvetica, Arial, "\5b8b\4f53", sans-serif;}*/
    .user-top{background-color: #fff; margin-top:-40px;box-shadow:0px 100px 50px rgba(145,145,145,.15);}
    #contants{width:1000px; background-color: #fff;height: 800px;}
    #mains{width:800px; float:right;height:600px;}
    #one{padding-left:30px;margin-top: 10px;}
    #two{width:200px;height:600px;border-right:1px solid #ccc;}
    #icon{width:200px;height:600px; padding-left: 10px;}
    #icon li{width:180px; height:30px; padding-left: 55px;line-height: 30px;border-radius:60px; margin-top: 10px; cursor:pointer;}
    #laji{width:1000px; height:50px;}
    /*#mains li{display: none;}*/
    #myGame{width:700; height:200px; border:1px solid #ccc; margin-left: 30px;box-shadow:0px 10px 5px #ccc;}
</style>
    <div class="container">
        <div class="user-main" id="user-main">

            <div class="user-top">
                <div class="user-top-info" style="margin-top:70px;">
                    
                    <div style="padding-top:10px; padding-left:40px;"><img src="/Public/img/tou-1.png"></div>
                    <div style="margin-left:220px; margin-top:-200px;"><img src="/Public/img/zb1.jpg" width="600px" height="200px"></div>
                    <a href="javascript:;" class="user-avatar-edit a9c btn-user-edit user-btn-password btn-orange" id="avatar-edit" title="修改头像" style="color:#000;">修改头像</a>

                    <div class="btn-sign btn-orange" id="boxReport" title="签到领积分">点我签到</div>

                    <ul class="user-shop-info">
                        <li class="user-info-1">信息完整：<span class="orange" id="completeness">0%</span><br>
                            <a class="a9c" href="/Public/center/37游戏用户中心_37游戏.html" title="完善信息" >完善&gt;</a>
                        </li>
                        <li class="user-info-2">剩余积分：<span class="orange" id="userpoint">2</span><br>
                            <a class="a9c" href="http://shop.37.com/task/" target="_blank" title="赚取积分">赚取&gt;</a>
                        </li>
                        <li class="user-info-3">会员级别：<span class="orange">VIP0</span><br>
                            <a class="a9c" href="http://vip.37.com/" target="_blank" title="会员体系">特权&gt;</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="mt20" id="contants">
                <div id="heads">
                <div id="laji"></div>
                    <ul id="mains">
                    <li  style="padding-left:50px;">
                        <h3><span class="glyphicon glyphicon-road" style="padding-left:30px"></span>修改密码</h3>
                        <form  action="<?php echo U('Grcenter/uppass');?>" method="post">
                            <div class="form-group mt50">
                                <label for="" class="col-sm-2 control-label">当前密码 ：</label>
                                <div class="col-sm-4">
                                    <input type="password" name="userpass" class="form-control" placeholder="旧的">
                                </div>
                            </div><br><br>
                            <div class="form-group">
                                <label for="" class="col-sm-2 control-label">新密码：</label>
                                <div class="col-sm-4">
                                    <input type="password" name="xuserpass" class="form-control" placeholder="新的">
                                    <input type="hidden" name="id" value="<?php echo ($Users["id"]); ?>">
                                </div>
                            </div><br><br>
<!--                             <div class="form-group">
                                <label for="" class="col-sm-2 control-label">确认新密码：</label>
                                <div class="col-sm-4">
                                    <input type="password" name="rxuserpass" class="form-control" placeholder="确认新的">
                                </div>
                            </div><br><br> -->
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-4">
                                  <button type="submit" class="btn btn-success btn-block">保存修改</button>
                                </div>
                            </div><br><br>
                        </form>
                    </li>
                    </ul>

                    <div id="two">
                        <ul id="icon">
                            <li><a href="<?php echo U('Grcenter/data');?>">个人资料</a></li>
                            <li><a href="<?php echo U('Grcenter/grcenter');?>">我的游戏</a></li>
                            <li style="background-color:#eee;"><a href="<?php echo U('Grcenter/alertpass');?>">修改密码</a></li>
                            <li>我的积分</li>
                            <li>我的消息</li>
                            <li>我的图库</li>
                        </ul>
                    </div>
                </div>
            </div><!--contands-->

        </div>
    </div><!--container-->
<!--block-->
   <!--footer-->
<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
		<div class="footer">
				<div class="container">
					<div class="footer-grids">
						<div class="col-md-12 footer-text">  
							<h3>007 Game</h3>
							<hr>
							<p>007游戏　版权所有　文化部网络游戏举报和联系电子邮箱：wlwh@vip.sina.com 纠纷处理方式：联系客服或依《用户协议》约定方式处理三七互娱旗下·上海硬通网络科技有限公司 沪网文[2014]0024-024号 沪ICP备14000728号-2 增值电信业务经营许可证沪B2-20140017</p>
							<a class="mt20" href="#">
								<img src="/Public/img/last1.jpg" width="171px" height="51px">
								<img src="/Public/img/last2.jpg" width="171px" height="51px">
								<img src="/Public/img/last3.jpg" width="171px" height="51px">
								<img src="/Public/img/last4.jpg" width="171px" height="51px">
								<img src="/Public/img/last5.jpg" width="171px" height="51px">
								<img src="/Public/img/last6.jpg" width="171px" height="51px">
							</a>
						</div>

						<div class="clearfix"> </div>
					</div>
					<div class="copy">
						<p>抵制不良单机游戏，拒绝盗版游戏。 注意自我保护，谨防受骗上当。 适度游戏益脑，沉迷游戏伤身。 合理安排时间，享受健康生活。</p>
						<p>&copy; 2016 007 小组 <a href="http://w3layouts.com/">007单机Game网</a> </p>
					</div>
				</div>
			</div>
			<!--start-smoth-scrolling-->
			<script type="text/javascript">
								jQuery(document).ready(function($) {
									$(".scroll").click(function(event){		
										event.preventDefault();
										$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
									});
								});
								</script>
							<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										// $().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>