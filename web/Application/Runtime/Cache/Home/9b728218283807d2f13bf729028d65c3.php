<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE HTML>
<html lang="cn">
<head>
<title>007单机游戏介绍网  用玩心创造世界</title>
<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jolly Games Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<link href="/Public/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="/Public/css/swipebox.css" rel='stylesheet' type='text/css' />
<link href="/Public/css/style.css" rel='stylesheet' type='text/css' />	
<link href="/Public/my.css" rel='stylesheet' type='text/css' />	
<!-- Custom Theme files -->
<script src="/Public/js/jquery.min.js"></script>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<style type="text/css">
	#bg1{
	    background-color: #eee;
	}
	#bg2{
		background-image: url('/Public/img/01.jpg');
	}
	#as{
	    width:230px;
	    height:40px;
	    background-color:#abcdef;
	}
	*{list-style: none;margin:0;padding: 0;}
	#header{width:650px; }
	#play{width:650px;  background-color: #fff;position: absolute;left:150px;top:190px;  z-index: 999999999999; border-left:1px solid #ddd;}
	#imgs{float: right;}
	#iconlist{ width:250px; height:260px; background-color: #fff;}
	#iconlist li{width:250px; height:52px; text-align: left; font-size: 18px; line-height: 50px;cursor: pointer;}

	#navs{background-color: #fff;height: 36px;width:100%; border-bottom: 2px solid #999;font-size: 15px;color:#000; line-height: 36px; cursor: pointer;}
</style>

</head>
<body>
	<audio controls="controls" autoplay="autoplay" style="display: none">
		  <!-- <source src="/Public/music.mp3" type="audio/ogg" /> -->
	</audio><!--音乐播放-->
	<!--start-home-->
	<?php if(empty($_SESSION)): ?><div id="navs">
			<div style="float:left; margin-left:40px;">
				<a href="#" style="color:#000;">首页</a>
			</div>
			<div style="float:left; margin-left:30px;">
				<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Enter/enter');?>" style="color:#000;">|&nbsp;登录</a>
			</div>
			<div style="float:right; margin-right:10px;">
				<a href="<?php echo U('Login/login');?>" style="color:#000;">注册</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
			</div>
		</div>
	<?php else: ?>
		<div id="navs">
			<div style="float:left; margin-left:40px;">
				<a href="#" style="color:#000;">首页</a>
			</div>
			<div style="float:left; margin-left:30px;">
				<a href="<?php echo U('Admin/User/index');?>" style="color:#000;">后台入口</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Sort/index');?>" style="color:#000;">所有游戏</a>
			</div>
			<div style="float:right; margin-right:60px;">
				<a href="<?php echo U('Index/loginout');?>" style="color:#000;">退出</a>
			</div>

			<div style="float:right; margin-right:40px;">
				<div style="color:#000;">
					<span style="color:#000;">欢迎您:</span>
					<a href="#" style="margin-right:50px;"><img src="/Public/img/avtar.png" width="30px" height="30px"><span  style="color:#000;"><?php echo ($_SESSION['home'][0]['username']); ?></span></a>
					<a href="<?php echo U('Grcenter/grcenter');?>" style="color:#000;">个人中心</a>
				</div>
			</div>
		</div><?php endif; ?>



	<div id="home" class="header">

		<div class="header-top">
			<div class="container">
				<div class="head-nav">
					<div class="logo">
						<a href="<?php echo U("Index/index");?>"><h1>007<span>单机Game<?php echo ($title); ?></span></h1></a>
					</div>
					<p>007单机游戏介绍网  用玩心创造世界</p>
					<div class="clearfix"></div>
					<span class="menu"></span>
					<div class="top-menu">
						<ul>
						<li class="active"><a class="color1" href="<?php echo U('Index/index');?>"  >首页</a></li>
							<li><a class="color2" href="<?php echo U('Sort/index');?>" id="yxx">游戏分类<span class="glyphicon glyphicon-chevron-down"></span></a></li>
							<li><a class="color5" href="<?php echo U('Gallery/gallery');?>">图库</a></li>
							<li><a class="color3" href="typography.html">玩家社区</a></li>
							<li><a href="##" class="color4">客服</a></li>
							<li><a class="color6" href="<?php echo U('Admin/Public/login');?>">后台入口</a></li>
							<div class="clearfix"> </div>
						</ul>
					</div>
					<!--script-for-menu-->
					<script>
						$( "span.menu" ).click(function() {
						  $( ".top-menu" ).slideToggle( "slow", function() {
						    // Animation complete.
						  });
						});
					</script>
					<!--script-for-menu-->
				</div>
			</div>
		</div> 

		<!-- banner-bottom -->
		<div class="banner">

				  <!--分类列表-->
				  <div id="header" style="display:none;">
				      <div id="play" >
				        <div id="imgs">
				            <a href="#"><img style="display:block" src="/Public/img/A1.jpg" width="400px" height="260px"></a>
				            <a href="#"><img style="display:none" src="/Public/img/A2.jpg" width="400px" height="260px"></a>
				            <a href="#"><img style="display:none" src="/Public/img/A3.jpg" width="400px" height="260px"></a>
				            <a href="#"><img style="display:none" src="/Public/img/A4.jpg" width="400px" height="260px"></a>
				            <a href="#"><img style="display:none" src="/Public/img/A5.jpg" width="400px" height="260px"></a>
				        </div>
				        <div>
				              <ul id="iconlist">
				                  <li>武侠仙侠</li>
				                  <li>角色扮演</li>
				                  <li>即时战略</li>
				                  <li>射击</li>
				                  <li>竞速</li>
				              </ul>
				        </div>
				      </div>
				  </div>
				  <!--分类列表-->
				  <script type="text/javascript">
				      $header = $('#header');
				      $iconlist = $('#iconlist li');
				      $imglist = $('#imgs img');

				      //控制heder  div
				      $('#yxx').mouseenter(function(){
				          $($header).slideToggle();
				        });

				      //设置循环 
				      var m = 0;

				      //控制图片 
				      function img(m){
				          for (var i =0; i < $imglist.length; i++) {
				              $imglist[i].style.display = 'none';
				          }
				          $imglist[m].style.display = 'block';
				      }

				      //控制按钮 
				      function icon(m){
				          for (var i =0; i < $iconlist.length; i++) {
				              $iconlist[i].style.backgroundColor = '#fff';
				          }
				          $iconlist[m].style.backgroundColor = '#eee';
				      }

				      //文字控制
				      for (var i = 0; i < $iconlist.length; i++) {
				          (function(i){
				              $iconlist[i].onmouseover = function(){
				                  img(i);
				                  icon(i);
				                  //改变循环变量
				                  m = i + 1;
				              }
				          })(i)
				      };
				  </script>
				  <!--分类列表-->
				  
    <!-- banner-bottom -->
    <div class="banner two">
    </div>
  <!--games-->
<!--  <div class="container">

    </div> -->

  <div class="gallery">
    <div class="container">

      <div class="portfolio-bottom">
        <div class="gallery-one" id="imglist">
            <ul id="options">
                <li class="active btn btn-display">全部图片</li>
                <li class="btn btn-display">角色扮演</li>
                <li class="btn btn-display">即时战略</li>
                <li class="btn btn-display">射击</li>
                <li class="btn btn-display">竞速</li>
            </ul>
            <ul id="cards" >
                <li class="active " ><!--武侠仙侠-->
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="col-md-4 gallery-left">
                    <a href="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                        <img src="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" alt="" class="img-responsive zoom-img"/ >
                        <a href="#">《<?php echo ($vo["iname"]); ?>》</a>
                    </a>
                  </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </li>

                <li><!--角色扮演-->
                        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="col-md-4 gallery-left">
                            <a href="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                                <img src="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" alt="" class="img-responsive zoom-img"/ >
                            </a>
                          </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </li>

                <li><!--即时战略-->
                        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="col-md-4 gallery-left">
                            <a href="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                                <img src="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" alt="" class="img-responsive zoom-img"/ >
                            </a>
                          </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </li>

                <li><!--射击-->
                        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="col-md-4 gallery-left">
                            <a href="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                                <img src="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" alt="" class="img-responsive zoom-img"/ >
                            </a>
                          </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </li>

                <li><!--竞速-->
                        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="col-md-4 gallery-left">
                            <a href="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>" class=" mask b-link-stripe b-animate-go   swipebox"  title="Image Title">
                                <img src="/Uploads/<?php echo ($vo["isp"]); ?>/<?php echo ($vo["isn"]); ?>"alt="" class="img-responsive zoom-img"/ >
                            </a>
                          </div><?php endforeach; endif; else: echo "" ;endif; ?>
                </li>
            </ul>
          </div>
        </div>

    </div>
  </div>
  <!--swipebox -->  
      <link rel="stylesheet" href="/Public/css/swipebox.css">
        <script src="/Public/js/jquery.swipebox.min.js"></script> 
        <script type="text/javascript">
          jQuery(function($) {
            $(".swipebox").swipebox();
          });
        </script>
      <!--//swipebox Ends -->
      <script type="text/javascript">
          //找对象  改属性
          var op = document.getElementById('options').getElementsByTagName('li');
          console.log(op);
          var ca = document.getElementById('cards').getElementsByTagName('li');
          console.log(ca);
          for (var i = 0; i < op.length; i++) {
            (function(i){
              op[i].onclick = function(){
                for(var m = 0; m<op.length; m++){
                  op[m].className = "btn btn-display";
                  ca[m].className = "";
                }
                op[i].className = 'active btn btn-display';
                ca[i].className = 'active';
              }
            })(i)
          };
      </script>
    <!--block-->
   <!--footer-->
<link rel="icon" href="/Public/img/title.png" type="image/png" sizes="16x16">
		<div class="footer">
				<div class="container">
					<div class="footer-grids">
						<div class="col-md-12 footer-text">  
							<h3>007 Game</h3>
							<hr>
							<p>007游戏　版权所有　文化部网络游戏举报和联系电子邮箱：wlwh@vip.sina.com 纠纷处理方式：联系客服或依《用户协议》约定方式处理三七互娱旗下·上海硬通网络科技有限公司 沪网文[2014]0024-024号 沪ICP备14000728号-2 增值电信业务经营许可证沪B2-20140017</p>
							<a class="mt20" href="#">
								<img src="/Public/img/last1.jpg" width="171px" height="51px">
								<img src="/Public/img/last2.jpg" width="171px" height="51px">
								<img src="/Public/img/last3.jpg" width="171px" height="51px">
								<img src="/Public/img/last4.jpg" width="171px" height="51px">
								<img src="/Public/img/last5.jpg" width="171px" height="51px">
								<img src="/Public/img/last6.jpg" width="171px" height="51px">
							</a>
						</div>

						<div class="clearfix"> </div>
					</div>
					<div class="copy">
						<p>抵制不良单机游戏，拒绝盗版游戏。 注意自我保护，谨防受骗上当。 适度游戏益脑，沉迷游戏伤身。 合理安排时间，享受健康生活。</p>
						<p>&copy; 2016 007 小组 <a href="http://w3layouts.com/">007单机Game网</a> </p>
					</div>
				</div>
			</div>
			<!--start-smoth-scrolling-->
			<script type="text/javascript">
								jQuery(document).ready(function($) {
									$(".scroll").click(function(event){		
										event.preventDefault();
										$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
									});
								});
								</script>
							<!--start-smoth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										// $().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>