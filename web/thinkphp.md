ThinkPHP
=====================================
1. 版本
    核心板
    完整版
    SAE版
    云引擎版
2. thinkPHP发布版本
    目前 5.0RC4
    thinkPHP 3.2 以后 要求php 5.3  大量的使用 命名空间
    thinkphp 3.2.3   以后开始 数据库全部使用PDO 

配置虚拟主机
    1.配置httpd.conf
        D:\wamp\bin\apache\apache2.4.17\conf
        1).开启重写模块
            LoadModule rewrite_module modules/mod_rewrite.so
        2). 访问权限 允许所有访问
            AllowOverride all
            Require all granted
        3).开启vhost配置文件(开启之后 默认的localhost就无法访问)
            Include conf/extra/httpd-vhosts.conf
    2. 配置vhost文件
        D:\wamp\bin\apache\apache2.4.17\conf\extra
        <VirtualHost *:80>
            DocumentRoot "d:/web"
            ServerName s47.com
        </VirtualHost>
    3.配置系统HOST
        C:\Windows\System32\drivers\etc

    PS.!(一定要重启服务)
    如果使用原来的 index.php文件 wampConfFile

ThinkPHP 快速入门
=====================================
一 基础
    1. 目录结构
        1.1 部署 目录结构
            www WEB部署目录（或者子目录）
            ├─index.php 入口文件
            ├─README.md README文件
            ├─composer.json Composer定义文件
            ├─Application 应用目录
            ├─Public 资源文件目录
            └─ThinkPHP 框架目录

        1.2 thinkPHP框架目录
            ├─ThinkPHP 框架系统目录（可以部署在非web目录下面）
            │ ├─Common 核心公共函数目录
            │ ├─Conf 核心配置目录
            │ ├─Lang 核心语言包目录
            │ ├─Library 框架类库目录
            │ │ ├─Think 核心Think类库包目录
            │ │ ├─Behavior 行为类库目录
            │ │ ├─Org Org类库包目录
            │ │ ├─Vendor 第三方类库目录
            │ │ ├─ ... 更多类库目录
            │ ├─Mode 框架应用模式目录
            │ ├─Tpl 系统模板目录
            │ ├─LICENSE.txt 框架授权协议文件
            │ ├─logo.png 框架LOGO文件
            │ ├─README.txt 框架README文件
            │ └─index.php 框架入口文件
        1.3 应用目录
            Application
            ├─Common 应用公共模块
            │ ├─Common 应用公共函数目录
            │ └─Conf 应用公共配置文件目录
            ├─Home 默认生成的Home模块
            │ ├─Conf 模块配置文件目录
            │ ├─Common 模块函数公共目录
            │ ├─Controller 模块控制器目录
            │ ├─Model 模块模型目录
            │ └─View 模块视图文件目录
            ├─Runtime 运行时目录
            │ ├─Cache 模版缓存目录
            │ ├─Data 数据目录
            │ ├─Logs 日志目录
            │ └─Temp 缓存目录模块设计
            模块/控制器/方法
    2. 入口文件
        安全文件
    3. 配置
        惯例配置  ThinkPHP里的配置
        应用配置  Application里的配置
        模块配置  Home里的配置
        动态配置  使用C()方法

    4. 控制器
        
    5. URL 请求（模式）
        普通模式
            http://s47.com/index.php?m=Home&c=Index&a=index
        PATHINFO模式
            http://s47.com/index.php/Home/Index/index
        rewrite模式
            http://s47.com/Home/User/index.html
        兼容模式
            http://s47.com/index.php?s=Home/Index/index
    6. 视图
        1.display()  可以省略参数
        2.访问不存在的方法 会先去找有没有 这个同名方法的模版文件
        3.绝对 不要 直接访问 模版文件!!

二  CURD
    1.数据创建
        create() 默认创建POST中数据,若非POST,传递数据create($data);
    2.数据写入
        add()  如果用create() 则添加创建好的数据.
            也可以给add()指定参数 (不建议)
    3.数据读取
        select()    返回二维数组
        find()      返回一维数组
        getField(字段名)  获取字段值
    4.更新数据
        save() 如果用create() 则更新创建好的数据.
            也可以给save()指定参数 (不建议)
        setField();
        setInc()   +
        setDec()   -
    5.删除
        delete();

三.控制器
    1.控制器定义
        实例化控制器 A()  R()
        多层控制器
        多级控制器
    2.前置和后置 操作
    3.操作的 参数的绑定
    4.伪静态
    5.URL生成
    6.Ajax返回
    7.跳转和重定向
        控制器的方法:
            $this->success()
            $this->error()
            $this->redirect()
        函数:
            redirect();
    8.输入变量
        I('变量类型.变量名/修饰符',['默认值'],['过滤方法或正则'],['额外数据源'])
        参1 过滤什么数据,使用斜线修饰符
        参2 如果值空,使用的默认值
        参3 过滤的函数, 可以写正则
    9.请求操作
    10.空操作 和  空控制器